@translation
Feature: Translation Feature Verify if user is able to translate his sentences in to the site

    @first
    Scenario Outline: Translation 2.3
        Given I open the Translation page
        When I write "<sentence>" in the translation input
        Then The translation output should equal "<translation>"

        Examples:
            | sentence | translation |
            | Danke    | Thank you   |

    @second
    Scenario Outline: Translation 2.4
        Given I open the Translation page
        When I change translation languages from "<source>" to "<target>"
        And I write "<sentence>" in the translation input
        Then The translation output should equal "<translation>"

        Examples:
            | sentence | translation | source | target |
            | Bonjour  | Hello       | fr     | en     |