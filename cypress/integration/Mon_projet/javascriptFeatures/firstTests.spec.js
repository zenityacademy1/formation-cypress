/// <reference types="cypress" />
const jsonAssertion = require("soft-assert")

beforeEach(() => { 
    cy.log('I run before every test') 
    cy.visit('/')
})

// describe('Front end testing', () => { 
//     it('Access the translate page', () => { 
//         cy.log('This is within my created test') 
//         cy.get('.dl_translator_page_container').should('be.visible') 
//         cy.get('.lmt__source_textarea').should((translateInput) => { 
//             expect(translateInput[0]).to.have.property('autocomplete', 'off') 
//         })
//     }) 
// })

describe('Exercice 2.2', () => { 
    it('Translation page access button', () => { 
        cy.visit('/press') 
        cy.get('#dl_menu_translator_simplified').click() 
        cy.reload()
        // cy.get('.lmt__source_textarea', { timeout: 10000 }).type('test')
        jsonAssertion.softAssert("a", "a") 
        jsonAssertion.softAssertAll()
        cy.log('suite')
    }) 
});

// describe('Exercice 2.2', () => { 
//     it('Translation page access button', () => { 
//         cy.get('.flex > [href="/api/table-of-contents"]').click()
//         // cy.url().should('equal', Cypress.config().baseUrl + 'api/table-of-contents') 
//     }) 
// });

describe('API tests in Pet Store', () => {
    it('Add a new Pet to the', () => {
        cy.request({
            method: 'POST',
            url: 'https://petstore.swagger.io/v2/pet',
            body: {
                "id": 0,
                "category": {
                    "id": 0,
                    "name": "dog"
                },
                "name": "lala",
                "photoUrls": [
                    "string"
                ],
                "tags": [
                    {
                    "id": 0,
                    "name": "dog"
                    }
                ],
                "status": "available"
            }
        }).then(response => { 
            cy.log(response.status) 
            cy.log(JSON.stringify(response.body)) 
        })
    })
})